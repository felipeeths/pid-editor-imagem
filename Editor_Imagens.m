function varargout = Editor_Imagens(varargin)
% EDITOR_IMAGENS MATLAB code for Editor_Imagens.fig
%      EDITOR_IMAGENS, by itself, creates a new EDITOR_IMAGENS or raises the existing
%      singleton*.
%
%      H = EDITOR_IMAGENS returns the handle to a new EDITOR_IMAGENS or the handle to
%      the existing singleton*.
%
%      EDITOR_IMAGENS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EDITOR_IMAGENS.M with the given input arguments.
%
%      EDITOR_IMAGENS('Property','Value',...) creates a new EDITOR_IMAGENS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Editor_Imagens_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Editor_Imagens_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Editor_Imagens

% Last Modified by GUIDE v2.5 30-May-2016 17:34:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;

gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Editor_Imagens_OpeningFcn, ...
                   'gui_OutputFcn',  @Editor_Imagens_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Editor_Imagens is made visible.
function Editor_Imagens_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Editor_Imagens (see VARARGIN)
global i;
global img_array;
i = 1;
img_array = [];
handles.image = 0;
handles.img_Original = 0;
img_array{i} = 0;   
% Choose default command line output for Editor_Imagens
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Editor_Imagens wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Editor_Imagens_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --------------------------------------------------------------------
function fspecial_roberts_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_roberts (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function Filters_Callback(hObject, eventdata, handles)
% hObject    handle to Filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function Help_Callback(hObject, eventdata, handles)
% hObject    handle to Help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function TextoEstatistica_Callback(hObject, eventdata, handles)
% hObject    handle to TextoEstatistica (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TextoEstatistica as text
%        str2double(get(hObject,'String')) returns contents of TextoEstatistica as a double

% --- Executes during object creation, after setting all properties.

function TextoEstatistica_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TextoEstatistica (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------
function Edit_Callback(hObject, eventdata, handles)
% hObject    handle to Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function Tools_Callback(hObject, eventdata, handles)
% hObject    handle to Tools (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Filtros_fspecial_Callback(hObject, eventdata, handles)
% hObject    handle to Filtros_fspecial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Filtros_Noise_Callback(hObject, eventdata, handles)
% hObject    handle to Filtros_Noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function About_Callback(hObject, eventdata, handles)
% hObject    handle to About (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
About;

% --------------------------------------------------------------------
function File_Close_Callback(hObject, eventdata, handles)
% hObject    handle to File_Close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all;


% --------------------------------------------------------------------
function Help_HelpME_Callback(hObject, eventdata, handles)
% hObject    handle to Help_HelpME (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Help;

% --------------------------------------------------------------------
% --- Executes during object creation, after setting all properties.
function Slider_Adjust_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Slider_Adjust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --------------------------------------------------------------------
% --------------------------------------------------------------------
% ------------------------ Codigo Funcional --------------------------
% --------------------------------------------------------------------
% --------------------------------------------------------------------
function File_Open_Callback(hObject, eventdata, handles)
% hObject    handle to File_Open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
i = i + 1;
[path,dir] = imgetfile();
if dir
	msgbox(sprintf('Error nenhuma imagem selecionada !'),'Error','Error');
	return
end
image = imread(path);
img_array{i} = image;
img_Original = image;
axes(handles.Image);
imshow(img_array{i});
handles.img_Original = img_Original;
guidata(hObject, handles);


% --------------------------------------------------------------------
function Edit_Redo_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_Redo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
i = i + 1;
try      
    imshow(img_array{i});
catch
    i = i - 1;
end

% --------------------------------------------------------------------
function File_Undo_Callback(hObject, eventdata, handles)
% hObject    handle to File_Undo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
i = i - 1;
try
    if img_array{i} == 0
       cla(handles.Image,'reset');
    else
      imshow(img_array{i});      
    end
catch
    i  = i + 1;
end

% --------------------------------------------------------------------
function Edit_ReturnImg_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_ReturnImg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_Original = handles.img_Original;
global img_array;
global i;
if img_Original == 0
    cla(handles.Image,'reset');
else
    axes(handles.Image);
    imshow(img_Original);
    i = i + 1;
    img_array{i} = img_Original;
end

% --------------------------------------------------------------------
function Filter_rgb2gray_Callback(hObject, eventdata, handles)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    try
        img_gray = rgb2gray(img_array{i});
        i = i + 1;
        img_array{i} = img_gray;
        imshow(img_array{i});
    catch
        msgbox('A Imagem precisa ser uma Matriz !','Error','Error');
    end
end

% --------------------------------------------------------------------
function Filters_GrayThresh_Callback(hObject, eventdata, handles)
% hObject    handle to Filters_GrayThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    level = graythresh(img_array{i});
    bw = im2bw(img_array{i},level);
    i = i + 1;
    img_array{i} = bw;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function File_New_Callback(hObject, eventdata, handles)
% hObject    handle to File_New_Callback (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
img_array = [];
i = 1;
img_array{i} = 0;
handles.img_Original = 0;
cla(handles.Image,'reset');
guidata(hObject, handles);

% --------------------------------------------------------------------
function File_Save_Callback(hObject, eventdata, handles)
% hObject    handle to File_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 F = getframe(handles.Image);
 Image = frame2im(F);
 [filename, ext, user_canceled] = imputfile;
 imwrite(Image,filename);
 Mensagem = {'Imagem salvada com sucesso'};
 msgbox(Mensagem,'Success','custom',Image);
 
% --- Executes on button press in buttonEstatistica.
function buttonEstatistica_Callback(hObject, eventdata, handles)
% hObject    handle to buttonEstatistica (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;

if img_array{i} == 0
    cla(handles.Image,'reset');
else
    level = graythresh(img_array{i});
    BW = im2bw(img_array{i},level);
    i = i + 1;
    img_array{i} = BW;
    imshow(img_array{i});
    cont = 0;
     for k = 1:size(BW,1)
         for j = 1:size(BW,2)
             pixel = BW(k,j);
             if pixel == 1
                 cont  = cont + 1; 
             end
         end
     end

    total = size(BW,1) * size(BW,2);
    porcentagem = (100 * cont) / total;
    porcentagem = 100 - porcentagem;
    saida = strcat(num2str(porcentagem),'%');
    set(handles.TextoEstatistica,'string',saida);
end


% --------------------------------------------------------------------
function Filters_PassaAlta_Callback(hObject, eventdata, handles)
% hObject    handle to Filters_PassaAlta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    filtro = 10*[-1/9 -1/9 -1/9; -1/9 8/9 -1/9; -1/9 -1/9 -1/9;]; %Passa Alta
  	image_filter = imfilter(img_array{i},filtro);
    i = i + 1;
    img_array{i} = image_filter;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function Filtros_PassaBaixa_Callback(hObject, eventdata, handles)
% hObject    handle to Filtros_PassaBaixa (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    filtro = [1/9 1/9 1/9; 1/9 1/9 1/9; 1/9 1/9 1/9;]; %Passa Baixa
    image_filter = imfilter(img_array{i},filtro);
    i = i + 1;
    img_array{i} = image_filter;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function Filters_AltoRef_Callback(hObject, eventdata, handles)
% hObject    handle to Filters_AltoRef (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    x = inputdlg({'Intensidade do Refor�o.'},'Intensidade', [1 30]);
    A = str2num(x{1});
    filtro = 10*[-1/9 -1/9 -1/9; -1/9 8/9 -1/9; -1/9 -1/9 -1/9;];
    image_filter = imfilter(img_array{i},filtro);
    imagem_reforco = (A-1)* img_array{i} + image_filter;
    i = i + 1;
    img_array{i} = imagem_reforco;
    imshow(img_array{i});
end


% --------------------------------------------------------------------
function filtros_imadjust_Callback(hObject, eventdata, handles)
% hObject    handle to filtros_imadjust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    try
       image_adjust = imadjust(img_array{i});
       i = i + 1;
       img_array{i} = image_adjust;
       imshow(img_array{i});
    catch
        msgbox('A Imagem tem que estar em escala de Cinza','Error','Error');
    end
end


% --------------------------------------------------------------------
function Tools_Hist_Callback(hObject, eventdata, handles)
% hObject    handle to Tools_Hist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
   cla(handles.Image,'reset');
else
  try
      setappdata(0,'image',img_array{i});
      Histogram;
  catch
      msgbox(sprintf('A Imagem t�m que estar em escala de cinza !'),'Error','Error');
  end
end


% --------------------------------------------------------------------
function Filtros_histadjust_Callback(hObject, eventdata, handles)
% hObject    handle to Filtros_histadjust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
   cla(handles.Image,'reset');
else
  try
      image = adapthisteq(img_array{i});
      setappdata(0,'image',img_array{i});
      i = i + 1;
      img_array{i} = image;
      imshow(img_array{i});
      Histogram;
  catch
      msgbox(({'Erro ao exibir o Histograma' 'Use "Ajustar Imagem" primeiro !'}),'Error','Error');
  end
end

% --------------------------------------------------------------------

% --- Executes on slider movement.
function Slider_Adjust_Callback(hObject, eventdata, handles)
% hObject    handle to Slider_Adjust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
Valor_Slider = get(hObject,'Value');
assignin('base','Valor_Slider',Valor_Slider);
set(handles.Edit_adjust,'String',num2str(Valor_Slider));
global img_array;
global i;
image_noise = imnoise(img_array{i},'salt & pepper',Valor_Slider);
i = i + 1;
img_array{i} = image_noise;
imshow(img_array{i});



function Edit_adjust_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_adjust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Edit_adjust as text
%        str2double(get(hObject,'String')) returns contents of Edit_adjust as a double

Valor_Text = get(hObject,'String');
Valor_Text = str2num(Valor_Text);
set(handles.Slider_Adjust,'Value',Valor_Text);
global img_array;
global i;
image_noise = imnoise(img_array{i},'salt & pepper',Valor_Text);
i = i + 1;
img_array{i} = image_noise;
imshow(img_array{i});


% --- Executes during object creation, after setting all properties.
function Edit_adjust_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_adjust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Noise_Salt_Callback(hObject, eventdata, handles)
% hObject    handle to Noise_Salt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    set(handles.Edit_adjust,'Enable','on');
    set(handles.Slider_Adjust,'Enable','on');
    set(handles.Edit_adjust,'String',get(handles.Slider_Adjust,'Value'));
    Valor_Slider = get(handles.Slider_Adjust,'Value');
    image_noise = imnoise(img_array{i},'salt & pepper',Valor_Slider);
    i = i + 1;
    img_array{i} = image_noise;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function Noise_Speckle_Callback(hObject, eventdata, handles)
% hObject    handle to Noise_Speckle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    set(handles.Edit_Speckle,'Enable','on');
    set(handles.Slider_Speckle,'Enable','on');
    set(handles.Edit_Speckle,'String',get(handles.Slider_Speckle,'Value'));
    Valor_Slider = get(handles.Slider_Speckle,'Value');
    image_noise = imnoise(img_array{i},'speckle',Valor_Slider);
    i = i + 1;
    img_array{i} = image_noise;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function Noise_Poisson_Callback(hObject, eventdata, handles)
% hObject    handle to Noise_Poisson (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    img_array{i} = im2double(img_array{i});
    Scale = 1e10;
    image_noise = Scale * imnoise(img_array{i}/Scale,'poisson');
    i = i +1;
    img_array{i} = image_noise;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function Noise_Localvar_Callback(hObject, eventdata, handles)
% hObject    handle to Noise_Localvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    image_noise = imnoise(img_array{i},'localvar',0.05*rand(size(img_array{i})));
    i = i +1;
    img_array{i} = image_noise;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function Noise_Gaussian_Callback(hObject, eventdata, handles)
% hObject    handle to Noise_Gaussian (see GCBO)
% eventdata  reserved - to be defined in a future version of
% MATLAB---------------
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    M = 0;
    V = 0.01;
    image_noise = imnoise(img_array{i},'gaussian',M,V);
    i = i + 1;
    img_array{i} = image_noise;
    imshow(img_array{i});
end

function Edit_Speckle_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_Speckle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Edit_Speckle as text
%        str2double(get(hObject,'String')) returns contents of Edit_Speckle as a double

% --- Executes during object creation, after setting all properties.
Valor_Text = get(hObject,'String');
Valor_Text = str2num(Valor_Text);
set(handles.Slider_Speckle,'Value',Valor_Text);
global img_array;
global i;
image_noise = imnoise(img_array{i},'speckle',Valor_Text);
i = i + 1;
img_array{i} = image_noise;
imshow(img_array{i});


function Edit_Speckle_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Edit_Speckle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function Slider_Speckle_Callback(hObject, eventdata, handles)
% hObject    handle to Slider_Speckle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
Valor_Slider = get(hObject,'Value');
assignin('base','Valor_Slider',Valor_Slider);
set(handles.Edit_Speckle,'String',num2str(Valor_Slider));
global img_array;
global i;
image_noise = imnoise(img_array{i},'speckle',Valor_Slider);
i = i + 1;
img_array{i} = image_noise;
imshow(img_array{i});


% --- Executes during object creation, after setting all properties.
function Slider_Speckle_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Slider_Speckle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --------------------------------------------------------------------
function fspecial_Prewitt_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_Prewitt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    prewitt = fspecial('prewitt');
    image = imfilter(img_array{i},prewitt);
    i = i + 1;
    img_array{i} = image;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function fspecial_Sobel_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_Sobel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    sobel = fspecial('sobel');
    image = imfilter(img_array{i},sobel);
    i = i + 1;
    img_array{i} = image;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function fspecial_Average_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_Average (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    h = [3 3];
    image_ave = fspecial('average',h);
    image = imfilter(img_array{i},image_ave);
    i = i + 1;
    img_array{i} = image;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function fspecial_Median_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_Median (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    try
        image = medfilt2(img_array{i});
        i = i + 1;
        img_array{i} = image;
        imshow(img_array{i});
    catch
        msgbox('A imagem tenque estar em escala de cinza','Error','Error');
    end
end
% --------------------------------------------------------------------
function fspecial_Disk_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_Disk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    image_disk = fspecial('disk',10);
    image = imfilter(img_array{i},image_disk,'replicate');
    i = i +1;
    img_array{i} = image;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function fspecil_Gaussian_Callback(hObject, eventdata, handles)
% hObject    handle to fspecil_Gaussian (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    image = imgaussfilt(img_array{i},2);
    i = i +1;
    img_array{i} = image;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function fspecial_log_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_log (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    filtro = [10 10];
    image_log = fspecial('log',filtro,0.5);
    image = imfilter(img_array{i},image_log);
    i = i +1;
    img_array{i} = image;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function fspecial_Motion_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_Motion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    image_motion = fspecial('motion',20,45);
    image = imfilter(img_array{i},image_motion,'replicate');
    i = i + 1;
    img_array{i} = image;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function Range_Callback(hObject, eventdata, handles)
% hObject    handle to Range (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    image = rangefilt(img_array{i});
    i = i + 1;
    img_array{i} = image;
    imshow(img_array{i});
end


% --------------------------------------------------------------------
function fspecial_unsharp_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_unsharp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    image_unsharp = fspecial('unsharp',0.2);
    image = imfilter(img_array{i},image_unsharp);
    i = i + 1;
    img_array{i} = image;
    imshow(img_array{i});
end


% --------------------------------------------------------------------
function fspecial_rob_vert_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_rob_vert (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    robver = [1,0;0,-1];
    image = imfilter(img_array{i},robver);
    i = i+1;
    img_array{i} = image;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function fspecial_robert_hor_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_robert_hor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    robhor = [0,1;-1,0];
    image = imfilter(img_array{i},robhor);
    i = i + 1;
    img_array{i} = image;
    imshow(img_array{i});
end

% --------------------------------------------------------------------
function fspecial_rob_soma_Callback(hObject, eventdata, handles)
% hObject    handle to fspecial_rob_soma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    robver = [1,0;0,-1];
    robhor = [0,1;-1,0];
    robfilterver = imfilter(img_array{i},robver);
    robfilterhor = imfilter(img_array{i},robhor);
    image = robfilterver + robfilterhor;
    i = i + 1;
    img_array{i} = image;
    imshow(img_array{i});
end


% --------------------------------------------------------------------
function tools_euclidiana_Callback(hObject, eventdata, handles)
% hObject    handle to tools_euclidiana (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
    cla(handles.Image,'reset');
else
    im = img_array{i};
    cor = uisetcolor;
    x = inputdlg({'Intensidade Euclidiana.'},'Intensidade', [1 30]);
    T = str2num(x{1});
    cor = cor * 255;
    im(1:30,1:30,1) = cor(1);
    im(1:30,1:30,2) = cor(2);
    im(1:30,1:30,3) = cor(3);
    ec = im;
    ec(:,:,:) = 0;

     for k = 1:size(im,1)
         for j = 1:size(im,2)
             d = sqrt((double((im(k,j,1) - cor(1))^2)) + (double((im(k,j,2) - cor(2))^2)) + (double((im(k,j,3) - cor(3))^2)));
             if (d < T)
                 ec(k,j,:) = im(k,j,:);
             end
         end
     end
     i = i + 1;
     imshow(ec);
     img_array{i} = ec;
     imshow(img_array{i});
end


% --------------------------------------------------------------------
function histeq_Callback(hObject, eventdata, handles)
% hObject    handle to histeq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img_array;
global i;
if img_array{i} == 0
   cla(handles.Image,'reset');
else
  try
      image = histeq(img_array{i});
      setappdata(0,'image',image);
      i = i + 1;
      img_array{i} = image;
      imshow(img_array{i});
      Histogram;
  catch
      msgbox(({'Erro ao exibir o Histograma precisa estar em escala de cinza !'}),'Error','Error');
  end
end
